<?php 

namespace App\Entities;
use CodeIgniter\Entity;

class EntViaje extends Entity 
{
	private $Ncliente;
    private $viajes;
    private $clientes;
    private $duracion;
    private $distancia;
    private $partida;
    private $final;
    private $tarifa;
    private $fec;
    private $estado;
    private $hora;

    

   

    /**
     * @return mixed
     */
    public function getNcliente()
    {
        return $this->Ncliente;
    }

    /**
     * @param mixed $Ncliente
     *
     * @return self
     */
    public function setNcliente($Ncliente)
    {
        $this->Ncliente = $Ncliente;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getViajes()
    {
        return $this->viajes;
    }

    /**
     * @param mixed $viajes
     *
     * @return self
     */
    public function setViajes($viajes)
    {
        $this->viajes = $viajes;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientes()
    {
        return $this->clientes;
    }

    /**
     * @param mixed $clientes
     *
     * @return self
     */
    public function setClientes($clientes)
    {
        $this->clientes = $clientes;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDuracion()
    {
        return $this->duracion;
    }

    /**
     * @param mixed $duracion
     *
     * @return self
     */
    public function setDuracion($duracion)
    {
        $this->duracion = $duracion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDistancia()
    {
        return $this->distancia;
    }

    /**
     * @param mixed $distancia
     *
     * @return self
     */
    public function setDistancia($distancia)
    {
        $this->distancia = $distancia;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPartida()
    {
        return $this->partida;
    }

    /**
     * @param mixed $partida
     *
     * @return self
     */
    public function setPartida($partida)
    {
        $this->partida = $partida;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFinal()
    {
        return $this->final;
    }

    /**
     * @param mixed $final
     *
     * @return self
     */
    public function setFinal($final)
    {
        $this->final = $final;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTarifa()
    {
        return $this->tarifa;
    }

    /**
     * @param mixed $tarifa
     *
     * @return self
     */
    public function setTarifa($tarifa)
    {
        $this->tarifa = $tarifa;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFec()
    {
        return $this->fec;
    }

    /**
     * @param mixed $fec
     *
     * @return self
     */
    public function setFec($fec)
    {
        $this->fec = $fec;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     *
     * @return self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * @param mixed $hora
     *
     * @return self
     */
    public function setHora($hora)
    {
        $this->hora = $hora;

        return $this;
    }
}